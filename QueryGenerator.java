package com.trendyol.oms.claimapi.repository.fts;

import com.couchbase.client.java.search.SearchQuery;
import com.couchbase.client.java.search.queries.AbstractFtsQuery;
import com.trendyol.oms.claimapi.model.request.ClaimSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class ClaimFullTextSearchQueryGenerator {
    private static final String CLAIM_FULL_TEXT_SEARCH_INDEX_NAME = "mpAdminIndex";

    public static SearchQuery generateQuery(ClaimSearchRequest claimSearchRequest) {
        ArrayList<AbstractFtsQuery> queries = new ArrayList<>();
        if (!StringUtils.isEmpty(claimSearchRequest.getFirstNamePrefix())) {
            queries.add(SearchQuery.regexp(claimSearchRequest.getFirstNamePrefix().toLowerCase() + ".*").field("customer.firstName"));
        }
        if (!StringUtils.isEmpty(claimSearchRequest.getLastNamePrefix())) {
            queries.add(SearchQuery.regexp(claimSearchRequest.getLastNamePrefix().toLowerCase() + ".*").field("customer.lastName"));
        }
        if (!StringUtils.isEmpty(claimSearchRequest.getOrderNumber())) {
            queries.add(SearchQuery.match(claimSearchRequest.getOrderNumber()).field("oNumber"));
        }
        if (!Objects.isNull(claimSearchRequest.getCargoTrackingNumber())) {
            queries.add(SearchQuery.numericRange()
                    .min(claimSearchRequest.getCargoTrackingNumber(), true)
                    .max(claimSearchRequest.getCargoTrackingNumber(), true)
                    .field("inPkgDetails.trackingNo"));
        }

        if (Objects.nonNull(claimSearchRequest.getClaimItemStatusNames()) && !claimSearchRequest.getClaimItemStatusNames().isEmpty()) {
            queries.add(SearchQuery.match(String.join(" ", claimSearchRequest.getClaimItemStatusNames())).field("items.status"));
        }

        queries.add(SearchQuery.numericRange()
                .min(claimSearchRequest.getSupplierId(), true)
                .max(claimSearchRequest.getSupplierId(), true)
                .field("supplierId"));
        queries.add(SearchQuery.numericRange()
                .min(claimSearchRequest.getFulfillmentTypeId(), true)
                .max(claimSearchRequest.getFulfillmentTypeId(), true)
                .field("ffTypeId"));

        AbstractFtsQuery ftsQuery = SearchQuery.conjuncts(queries.toArray(new AbstractFtsQuery[queries.size()]));
        SearchQuery searchQuery = new SearchQuery(CLAIM_FULL_TEXT_SEARCH_INDEX_NAME, ftsQuery);
        searchQuery.limit(claimSearchRequest.getSize());
        searchQuery.skip(claimSearchRequest.getPage() * claimSearchRequest.getSize());
        searchQuery.sort("-createdDate");
        return searchQuery;
    }
}
