@Repository
public class ClaimRepository {
    
    private final CouchbaseTemplate claimCouchbaseTemplate;
    private final ClaimDocumentConverter claimDocumentConverter;

    public ClaimRepository(@Qualifier("claimCouchbaseTemplate") CouchbaseTemplate claimCouchbaseTemplate,
                           ClaimDocumentConverter claimDocumentConverter) {
        this.claimCouchbaseTemplate = claimCouchbaseTemplate;
        this.claimDocumentConverter = claimDocumentConverter;
    }
   
    public ClaimSearchResultDto search(SearchQuery ftsQuery) {
        SearchQueryResult queryResult = claimCouchbaseTemplate.getCouchbaseBucket().query(ftsQuery);
        long totalHits = queryResult.metrics().totalHits();
        List<UUID> claimIds = queryResult.hits().stream().map(searchQueryRow -> UUID.fromString(searchQueryRow.id())).collect(Collectors.toList());
        return new ClaimSearchResultDto(totalHits, claimIds);
    }

    public List<Claim> getBulkByIdList(List<UUID> claimIds) {

        Statement statement = select(CLAIM_SELECT_WITH_META)
                .from(i("claim"))
                .useKeysValues(claimIds.stream().map(Object::toString).toArray(String[]::new));

        List<ClaimDocument> claims = fetchClaimList(statement);

        return claims
                .stream()
                .map(claimDocumentConverter::toDomainModel)
                .collect(Collectors.toList());
    }

}
